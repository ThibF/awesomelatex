FROM blang/latex

WORKDIR /root
## Latex package manager setup
RUN apt-get update && apt-get install -y wget texlive-full fonts-font-awesome

## Update latex font cache with new fonts
RUN luaotfload-tool -v -u

WORKDIR /data